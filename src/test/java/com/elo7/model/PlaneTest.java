package com.elo7.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PlaneTest {

	@Test
	public void should_return_true_when_position_is_contained() {
		Plane plane = new Plane(10, 10);
		assertTrue(plane.contains(new Position(5, 5)));
	}

	@Test
	public void should_return_false_when_position_is_negative() {
		Plane plane = new Plane(10, 10);
		assertFalse(plane.contains(new Position(-1, -1)));
	}

	@Test
	public void should_return_false_when_position_is_out_of_width_bounds() {
		Plane plane = new Plane(10, 10);
		assertFalse(plane.contains(new Position(10, 5)));
	}

	@Test
	public void should_return_false_when_position_is_out_of_height_bounds() {
		Plane plane = new Plane(10, 10);
		assertFalse(plane.contains(new Position(5, 10)));
	}

	@Test
	public void should_return_true_when_position_is_available() throws Exception {
		Plane plane = new Plane(10,10);
		assertTrue(plane.isAvailable(new Position(5,  5)));
	}

	@Test
	public void should_return_false_when_position_is_unavailable() throws Exception {
		Plane plane = new Plane(10,10);
		new Rover().land(new Position(5, 5), Orientation.NORTH, plane);

		assertFalse(plane.isAvailable(new Position(5,  5)));
	}

}
