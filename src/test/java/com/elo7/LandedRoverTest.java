package com.elo7;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import com.elo7.model.Direction;
import com.elo7.model.LandedRover;
import com.elo7.model.Orientation;
import com.elo7.model.Plane;
import com.elo7.model.Position;
import com.elo7.model.Rover;

public class LandedRoverTest {

	private Plane plane;

	@Before
	public void setup() {
		plane = new Plane(10, 10);
	}

	@Test
	public void should_face_west_when_turn_left_and_orientation_is_north() {
		Rover rover = new Rover();
		LandedRover landedRover = rover.land(new Position(2, 2), Orientation.NORTH, plane);
		landedRover.turn(Direction.LEFT);

		assertEquals(Orientation.WEST, landedRover.getOrientation());
	}

	@Test
	public void should_face_north_when_turn_left_and_orientation_is_east() {
		Rover rover = new Rover();
		LandedRover landedRover = rover.land(new Position(2, 2), Orientation.EAST, plane);
		landedRover.turn(Direction.LEFT);

		assertEquals(Orientation.NORTH, landedRover.getOrientation());
	}

	@Test
	public void should_face_east_when_turn_left_and_orientation_is_south() {
		LandedRover rover = new Rover().land(new Position(2, 2), Orientation.SOUTH, plane);
		rover.turn(Direction.LEFT);

		assertEquals(Orientation.EAST, rover.getOrientation());
	}

	@Test
	public void should_face_south_when_turn_left_and_orientation_is_west() {
		LandedRover rover = new Rover().land(new Position(2, 2), Orientation.WEST, plane);
		rover.turn(Direction.LEFT);

		assertEquals(Orientation.SOUTH, rover.getOrientation());
	}

	@Test
	public void should_face_east_when_turn_right_and_orientation_is_north() {
		LandedRover rover = new Rover().land(new Position(2, 2), Orientation.NORTH, plane);
		rover.turn(Direction.RIGHT);

		assertEquals(Orientation.EAST, rover.getOrientation());
	}

	@Test
	public void should_face_south_when_turn_right_and_orientation_is_east() {
		LandedRover rover = new Rover().land(new Position(2, 2), Orientation.EAST, plane);
		rover.turn(Direction.RIGHT);

		assertEquals(Orientation.SOUTH, rover.getOrientation());
	}

	@Test
	public void should_face_west_when_turn_right_and_orientation_is_south() {
		LandedRover rover = new Rover().land(new Position(2, 2), Orientation.SOUTH, plane);
		rover.turn(Direction.RIGHT);

		assertEquals(Orientation.WEST, rover.getOrientation());
	}

	@Test
	public void should_face_north_when_turn_right_and_orientation_is_west() {
		LandedRover rover = new Rover().land(new Position(2, 2), Orientation.WEST, plane);
		rover.turn(Direction.RIGHT);

		assertEquals(Orientation.NORTH, rover.getOrientation());
	}

	@Test
	public void should_increment_y_when_orientation_is_north() {
		LandedRover rover = new Rover().land(new Position(2, 2), Orientation.NORTH, plane);
		rover.move();

		assertEquals(new Position(2, 3), rover.getPosition());
	}

	@Test
	public void should_increment_x_when_orientation_is_east() {
		LandedRover rover = new Rover().land(new Position(2, 2), Orientation.EAST, plane);
		rover.move();

		assertEquals(new Position(3, 2), rover.getPosition());
	}

	@Test
	public void should_decrement_y_when_orientation_is_south() {
		LandedRover rover = new Rover().land(new Position(2, 2), Orientation.SOUTH, plane);
		rover.move();

		assertEquals(new Position(2, 1), rover.getPosition());
	}

	@Test
	public void should_decrement_X_when_orientation_is_west() {
		LandedRover rover = new Rover().land(new Position(2, 2), Orientation.WEST, plane);
		rover.move();

		assertEquals(new Position(1, 2), rover.getPosition());
	}

	@Test
	public void should_not_move_rover_if_position_is_not_valid(){
		Plane plane = new Plane(5, 5);
		LandedRover rover = new Rover().land(new Position(4, 4), Orientation.EAST, plane);

		assertFalse(rover.move());
	}
}
