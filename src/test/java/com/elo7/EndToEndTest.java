package com.elo7;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.elo7.model.Command;
import com.elo7.model.LandedRover;
import com.elo7.model.Orientation;
import com.elo7.model.Plane;
import com.elo7.model.Position;
import com.elo7.model.Rover;

public class EndToEndTest {

	@Test
	public void should_move_rover_from_begin_to_end_of_plane() throws Exception {
		Plane plane = new Plane(5, 5);
		Rover rover = new Rover();
		LandedRover landedRover = rover.land(new Position(1, 2), Orientation.NORTH, plane);
		CommandParser commandParser = new CommandParser();
		List<Command> commands = commandParser.parse("LMLMLMLMM");
		commands.forEach(landedRover::execute);

		assertEquals(new Position(1, 3), landedRover.getPosition());
		assertEquals(Orientation.NORTH, landedRover.getOrientation());
	}

}
