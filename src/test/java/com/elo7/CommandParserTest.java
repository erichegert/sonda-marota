package com.elo7;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

import com.elo7.model.Command;

public class CommandParserTest {

	@Test
	public void should_return_MOVE_command_for_alias_M() {
		List<Command> parse = new CommandParser().parse("M");

		Assert.assertThat(parse, Matchers.contains(Command.MOVE));
	}

	@Test
	public void should_return_TURN_LEFT_command_for_alias_L() {
		List<Command> parse = new CommandParser().parse("L");

		Assert.assertThat(parse, Matchers.contains(Command.TURN_LEFT));
	}

	@Test
	public void should_return_TURN_RIGHT_command_for_alias_R() {
		List<Command> parse = new CommandParser().parse("R");

		Assert.assertThat(parse, Matchers.contains(Command.TURN_RIGHT));
	}

	@Test
	public void should_return_commands_in_order() {
		List<Command> parse = new CommandParser().parse("LRM");

		Assert.assertThat(parse, Matchers.contains(Command.TURN_LEFT, Command.TURN_RIGHT, Command.MOVE));
	}

	@Test
	public void should_return_absent_command_if_invalid_alias() {
		List<Command> parse = new CommandParser().parse("Z");

		Assert.assertThat(parse, Matchers.empty());
	}

	@Test
	public void should_remove_invalid_command_from_command_stream() {
		List<Command> parse = new CommandParser().parse("LRZM");

		Assert.assertThat(parse, Matchers.contains(Command.TURN_LEFT, Command.TURN_RIGHT, Command.MOVE));
	}

}
