package com.elo7;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.elo7.model.Command;
import com.elo7.model.LandedRover;
import com.elo7.model.Orientation;
import com.elo7.model.Plane;
import com.elo7.model.Position;
import com.elo7.model.Rover;

public class CommandTest {

	Plane plane;

	@Before
	public void setup() {
		plane = new Plane(10, 10);
	}

	@Test
	public void should_move_rover_when_command_is_move() {
		Command subject = Command.MOVE;
		Rover rover = new Rover();
		LandedRover landedRover = rover.land(new Position(5, 5), Orientation.NORTH, plane);
		landedRover.execute(subject);
		assertEquals(new Position(5, 6), landedRover.getPosition());
		assertEquals(Orientation.NORTH, landedRover.getOrientation());
	}

	@Test
	public void should_turn_rover_to_right_when_command_is_turn_right() {
		Command subject = Command.TURN_RIGHT;
		Rover rover = new Rover();
		LandedRover landedRover = rover.land(new Position(5, 5), Orientation.NORTH, plane);
		landedRover.execute(subject);
		assertEquals(new Position(5, 5), landedRover.getPosition());
		assertEquals(Orientation.EAST, landedRover.getOrientation());
	}

	@Test
	public void should_turn_rover_to_left_when_command_is_turn_left() {
		Command subject = Command.TURN_LEFT;
		Rover rover = new Rover();
		LandedRover landedRover = rover.land(new Position(5, 5), Orientation.NORTH, plane);
		landedRover.execute(subject);
		assertEquals(new Position(5, 5), landedRover.getPosition());
		assertEquals(Orientation.WEST, landedRover.getOrientation());
	}

}
