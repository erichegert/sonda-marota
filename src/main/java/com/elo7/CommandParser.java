package com.elo7;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import com.elo7.model.Command;

public class CommandParser {
	public List<Command> parse(String instructions){
		String[] split = instructions.split("");
		return asList(split).stream()
				.map(Command::forAlias)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(toList());
	}
}
