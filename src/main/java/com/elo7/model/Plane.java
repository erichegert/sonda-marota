package com.elo7.model;

import java.util.List;
import java.util.Vector;

public class Plane {

	private final int width;
	private final int height;
	private final List<LandedRover> rovers = new Vector<>();

	public Plane(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public boolean contains(Position nextPosition) {
		return nextPosition.getX() < width && nextPosition.getY() < height &&
				nextPosition.getX() >= 0 && nextPosition.getY() >= 0;
	}

	void addRover(LandedRover landedRover){
		rovers.add(landedRover);
	}

	public boolean isAvailable(Position nextPosition) {
		return !rovers.stream().anyMatch(r -> r.getPosition().equals(nextPosition));
	}
}
