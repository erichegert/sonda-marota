package com.elo7.model;

public class LandedRover {

	private Orientation orientation;
	private final Plane plane;
	private Position position;

	LandedRover(Position initialPosition, Orientation orientation, Plane plane) {
		this.plane = plane;
		this.position = initialPosition;
		this.orientation = orientation;
		plane.addRover(this);
	}

	public void turn(Direction direction) {
		orientation = orientation.turn(direction);
	}

	public boolean move() {
		Position nextPosition = position.add(orientation.getNextPosition());

		if(!plane.contains(nextPosition)){
			return false;
		}
		if(!plane.isAvailable(nextPosition)){
			return false;
		}
		position = nextPosition;
		return true;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	public Position getPosition() {
		return this.position;
	}

	public void execute(Command command) {
		command.execute(this);
	}
}
