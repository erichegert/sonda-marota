package com.elo7.model;

import static java.util.Arrays.asList;

import java.util.Optional;
import java.util.function.Consumer;

public enum Command {
	MOVE("M", r -> r.move()),
	TURN_LEFT("L", r -> r.turn(Direction.LEFT)),
	TURN_RIGHT("R", r -> r.turn(Direction.RIGHT));

	private final String alias;
	private final Consumer<LandedRover> action;

	private Command(String alias, Consumer<LandedRover> action) {
		this.alias = alias;
		this.action = action;
	}

	public static Optional<Command> forAlias(String alias){
		return asList(values())
				.stream()
				.filter(c -> c.alias.equals(alias))
				.findFirst();
	}

	void execute(LandedRover rover) {
		action.accept(rover);
	}

}
