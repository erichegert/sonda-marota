package com.elo7.model;

public enum Orientation {
	NORTH("N", new Position(0, 1)),
	EAST("E", new Position(1, 0)),
	SOUTH("S", new Position(0, -1)),
	WEST("W", new Position(-1, 0));

	private final String discriminator;
	private final Position nextPosition;

	private Orientation(String discriminator, Position nextPosition) {
		this.discriminator = discriminator;
		this.nextPosition = nextPosition;
	}

	public Position getNextPosition(){
		return nextPosition;
	}

	public Orientation getLeft(){
		Orientation[] values = values();
		int position = ordinal();
		return values[(position-1+values.length) % values.length];
	}

	public Orientation getRight(){
		Orientation[] values = values();
		int position = ordinal();
		return values[(position+1) % values.length];
	}

	public Orientation turn(Direction direction) {
		if (direction == Direction.LEFT)
			return getLeft();
		return getRight();
	}
}
