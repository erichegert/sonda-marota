package com.elo7.model;

public class Rover {

	public LandedRover land(Position position, Orientation orientation, Plane plane){
		return new LandedRover(position, orientation, plane);
	}

	public boolean move(){
		System.out.println("Movendo a rodinha");
		return true;
	}
}
