package com.elo7;

import java.util.List;

import com.elo7.model.Command;
import com.elo7.model.LandedRover;
import com.elo7.model.Orientation;
import com.elo7.model.Plane;
import com.elo7.model.Position;
import com.elo7.model.Rover;

public class Application {

	public static void main(String[] args) {
		final String commands = "MMRMMRMRRM";
		List<Command> parsedCommands = new CommandParser().parse(commands);
		Plane plane = new Plane(20, 20);
		Rover rover = new Rover();
		LandedRover landedRover = rover.land(new Position(1, 1), Orientation.SOUTH, plane);
		for (Command command : parsedCommands) {
			landedRover.execute(command);
		}


	}

}
